---
title: ""
date: 2018-05-06T11:40:10+02:00
---
# What if ...
- You create multiple buildings by one way?
- Create residential area by selecting the buildings?

![Mapathoner demo][1]

See the project on [GitLab][2] or translate on [Transifex][3].

[1]: https://upload.wikimedia.org/wikipedia/commons/9/9f/Mapathoner_demo.gif
[2]: https://gitlab.com/qeef/mapathoner
[3]: https://www.transifex.com/josm/josm/content/
