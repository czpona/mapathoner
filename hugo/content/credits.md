---
title: "Credits"
date: 2018-05-06T11:27:40+02:00
---
And what about the history of the plugin? It evolved from [JOSM scripts][1].

## [Easy buildings][2] - autumn 2016
Credits: It's me.

This was the first attempt to deal with my laziness. More than a few
clicks and one shortcut keypress is no way. [BuildingsTools][3] make the job
for square buildings but not for circle (and I do not copy + resize them
because for me it's too heavy too) nor for residential areas.

## [Pick residential][4] - 2017-03-28, [SVĚT-HUB][5]
Credits: [@Piskvor][6]

We discussed [Easy buildings][2] that was not usable for [@Piskvor][6] because it
does not fit to his workflow. However, he proposed some script that would
make residential area around buildings on the screen. Not exactly what he
wanted (buildings have to be selected before script run) but better than
nothing.

## [Batch buildings][7] - 2017-09-26
Credits: [@marxin][8]

This feature fits to [@marxin][8]'s workflow - just click as many buildings of
same kind as possible. It works for batch of circle buildings and for batch of
orthogonal buildings. Backward compatibility ensured and it was not such big
pain.

What is new with this feature is that collaboration on github slowly
establishes.

## [Mapathoner plugin][9] - 2018-04-24
Credits: [@marxin][8]

We discussed [@marxin][8]'s contribution to [JOSM][10] that was accepted (hurray!)
and the conversation goes around if the [JOSM scripts][1] could be rewritten to
Java and pushed upstream also. However scripts are pretty specific so I decided
to develop [JOSM plugin][11] instead.

I named the plugin *Mapathoner* after 3 days bad sleeping thinking about the
name. I wanted to avoid direct connection to [HOT][12] or [Missing Maps][13]
because of potential legal issues. And in fact - the mapathons are where we
thought and discussed the scripts, where the scripts were born. Where the
*Mapathoner* plugin was born.

## Languages, 2018-06-06
Credits: [@floscher][14]

A lot changed during last two days. Anywawy,
[the project has moved to GitLab][15]. Thanks to [@floscher][14], who points me
out, I set up automatic builds. And, again thanks to [@floscher][14], [Mapathoner
is part of JOSM on Transifex][16] now.

## [L-shaped buildings][17] - 2018-06-05, [Pracovna][18]
Credits: [disastermappers heidelberg][19] member whose name I'm trying to find out

Just after successfull presentation of
[The Czech Contribution to Missing Maps][20] that contained translations of
Missing Maps web page and Highway Tag Africa, JOSM patches and the Mapathoner
plugin for JOSM I discussed improvement of the Mapathoner with that guy from
[disastermappers heidelberg][19]. Well, it takes too long to create L-shaped
building! Or, in fact, it took.

## [Pick residential][4] again - 2019-05-07
Credits: [@Piskvor][6]

Well, it took a little bit longer than it should. Most probably because I think
it would not be easy. But why it shouldn't be? So, now you can make residential
area around buildings on the screen. No need to select them any more!

[1]: https://gitlab.com/qeef/josm-scripts
[2]: https://gitlab.com/qeef/josm-scripts/blob/master/doc/user/easy_buildings.md
[3]: https://wiki.openstreetmap.org/wiki/JOSM/Plugins/BuildingsTools
[4]: https://gitlab.com/qeef/josm-scripts/blob/master/doc/user/pick_residential.md
[5]: http://www.svet-hub.cz/
[6]: https://github.com/piskvor
[7]: https://github.com/qeef/josm-scripts/issues/11
[8]: https://github.com/marxin
[9]: http://qeef.gitlab.io/mapathoner/
[10]: https://josm.openstreetmap.de/
[11]: https://josm.openstreetmap.de/wiki/Plugins
[12]: https://www.hotosm.org/
[13]: http://www.missingmaps.org/
[14]: https://gitlab.com/floscher
[15]: https://gitlab.com/qeef/mapathoner
[16]: https://www.transifex.com/josm/josm/josm-plugin_mapathoner/
[17]: usage
[18]: http://pracovna.cz/
[19]: https://disastermappers.wordpress.com/
[20]: https://qeef.gitlab.io/talks/the-czech-contribution-to-missing-maps/
