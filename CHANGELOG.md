# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][] and this project adheres to
[Semantic Versioning][].

[Keep a Changelog]: http://keepachangelog.com/
[Semantic Versioning]: http://semver.org/

## [Unreleased][]

## [0.10.1][] - 2019-10-01
### Fixed
- Minimum JOSM version.

## [0.10.0][] - 2019-09-30
### Changed
- Refactor the code, make it prettier.

### Fixed
- Update to JOSM 15390, see #13.

## [0.9.8][] - 2019-09-09
### Added
- Japanese translations for plugin.

## [0.9.7][] - 2019-08-13
- Update translations.

## [0.9.6][] - 2019-07-31
### Fixed
- Error in dot product computation #12.

## [0.9.5][] - 2019-07-29
### Fixed
- IOOBE at `PickResidentialAreaAction.find_ltbr` #11.

## [0.9.4][] - 2019-07-10
### Fixed
- Workaround for missing `_index` of `zh_CN`.

## [0.9.3][] - 2019-07-09
### Added
- Chinese Simplified (China) `zh_CN` language.

## [0.9.2][] - 2019-06-18
### Added
- Update translations.

## [0.9.1][] - 2019-05-20
### Added
- Update translations.

## [0.9.0][] - 2019-05-07
### Added
- When *Pick Residential Area* is run and nothing is selected, all the
  buildings currently seen are considered as part of the new residential area.

### Changed
- Run CI/CD only if new tag is pushed.

## [0.8.7][] - 2019-04-28
### Fixed
- Deprecated `OsmPrimitive.getFilteredList`, use `Utils.filteredCollection`.
- Deprecated `MainApplication.undoRedo`, use `UndoRedoHandler.getInstance()`.

## [0.8.6][] - 2019-03-31
### Added
- Korean language.

## [0.8.5][] - 2019-03-24
### Added
- Update Italian translations.

## [0.8.4][] - 2019-03-08
### Fixed
- Node is already deleted issue #7.
- No nodes for Pick Residential.

## [0.8.3][] - 2019-03-04
### Fixed
- Install `gettext` before build.

## [0.8.2][] - 2019-03-04
### Fixed
- Fix `-dirty` version.

## [0.8.1][] - 2019-03-03
### Changed
- Upgrade Gradle to 5.2.1.

## [0.8.0][] - 2019-03-03
### Changed
- Update Gradle to 5.1 and gradle-josm-plugin to v0.6.1.

## [0.7.8][] - 2019-02-18
### Fixed
- Pick Residential Area for all `buildings=*`, see #6.

## [0.7.7][] - 2018-12-17
### Added
- Hungarian translations for pages.

## [0.7.6][] - 2018-11-28
### Added
- Czech translations.

## [0.7.5][] - 2018-11-26
### Changed
- Bump Greek, Chinese (Taiwan).

## [0.7.4][] - 2018-11-12
### Added
- Chinese (Taiwan) translation for plugin.

## [0.7.3][] - 2018-11-09
### Fixed
- Bad script order in GitLab CI/CD config file.

## [0.7.2][] - 2018-11-09
### Added
- Chinese (Taiwan) translations for pages.

## [0.7.1][] - 2018-11-01
### Added
- Persian translation of index page.

## [0.7.0][] - 2018-10-28
### Changed
- The rest of the links in pages (pictures including) changed to numbered. It
  should be easier now to understand what to translate - all except links
  numbers.

### Fixed
- Missing release dates in changelog.

## [0.6.5][] - 2018-10-28
### Changed
- Links in hugo pages to numbered (minimize errors in translations).

## [0.6.4][] - 2018-10-05
### Added
- Spanish translations for pages.

## [0.6.3][] - 2018-10-05
### Added
- Script for creating not translated pages.

### Fixed
- If usage/credits files not translated, create blank ones with link to
  transifex. This resolves [#5][].

[#5]: https://gitlab.com/qeef/mapathoner/issues/5

## [0.6.2][] - 2018-10-02
### Added
- Portuguese translations for pages.

## [0.6.1][] - 2018-09-17
### Changed
- Update to Gradle JOSM plugin version 0.5.0.

### Fixed
- Remove code dependent on `Main`.

## [0.6.0][] - 2018-09-15
### Changed
- Design of languages list in hugo pages.

### Fixed
- Some typo in *usage*, *credits* pages.

## [0.5.17][] - 2018-09-07
### Changed
- Bump German translations.

## [0.5.16][] - 2018-08-27
### Changed
- Bump Italian translations.

## [0.5.15][] - 2018-08-24
### Added
- Italian translations.

## [0.5.14][] - 2018-08-23
### Changed
- Bump Russian translations.

## [0.5.13][] - 2018-08-06
### Added
- Polish translations.

## [0.5.12][] - 2018-06-27
### Changed
- Bump Greek translations.

## [0.5.11][] - 2018-06-27
### Fixed
- Missing gettext package in GitLab CI/CD.

## [0.5.10][] - 2018-06-27
### Added
- Greek translations for pages.

## [0.5.9][] - 2018-06-26

## [0.5.8][] - 2018-06-24
### Added
- German translations for pages.

## [0.5.7][] - 2018-06-22
### Fixed
- Language-dependent link to home.
- Last building not created bug.

## [0.5.6][] - 2018-06-22
### Changed
- Revert d1526c5 because Hugo creates `-d` output folder relative to `-s`
  source folder.

### Fixed
- Header partial of Hugo Coder theme, use `.Title` for `.Page`.

## [0.5.5][] - 2018-06-22
### Fixed
- Link to JavaDoc in Hugo Coder theme header.

## [0.5.4][] - 2018-06-22
### Added
- Russian translations for pages.

### Changed
- Change Hugo Coder theme to automatically show available content. This solve
  the problem with i18n because page not available in desired language is not
  shown.

### Fixed
- GitLab CI/CD `./public` folder location.

## [0.5.3][] - 2018-06-22
### Added
- Default content language.

## [0.5.2][] - 2018-06-21
### Added
- Transifex translation info on intro page.

### Fixed
- Closed way error.

## [0.5.1][] - 2018-06-20
### Fixed
- Multilingual navigation on all pages.

## [0.5.0][] - 2018-06-20
### Added
- Transifex push/pull settings for Hugo content.

### Changed
- Hugo Coder theme include multilingual.

### Fixed
- Markdown documentation links syntax.

## [0.4.2][] - 2018-06-11
### Fixed
- GitLab CI apt-get update.

## [0.4.1][] - 2018-06-11
### Fixed
- GitLab CI to include transifex when release.

## [0.4.0][] - 2018-06-11
### Added
- Batch L-shaped building action.
- The *mapathoner* plugin is already translated on [Transifex][] to the
  following languages:
  - English (en)[source language]
  - German (de)
  - Hungarian (hu)
  - Russian (ru)

### Changed
- Update Gradle JOSM plugin.
- Upload translations to Transifex.
- Automatically download translations from Transifex when building `.jar`.

[Transifex]: https://www.transifex.com/josm/josm/josm-plugin_mapathoner/

## [0.3.0][] - 2018-06-06
### Changed
- Project moved to GitLab.
- Update Gradle JOSM plugin by [@floscher][].

[@floscher]: https://gitlab.com/floscher

## [0.2.4][] - 2018-05-10
### Fixed
- Undo/Redo for Pick Residential Area action.
- Undo/Redo for Batch Orthogonalize Building action.
- Undo/Redo for Batch Circle Building action.

## [0.2.3][] - 2018-05-08
### Fixed
- Nodes count check for circle and orthogonal batch buildings.

## [0.2.2][] - 2018-05-06
### Fixed
- Navigation title link in Hugo Coder theme.

## [0.2.1][] - 2018-05-06
### Fixed
- Https in baseURL link.

## [0.2.0][] - 2018-05-06
### Added
- Mapathoner package JavaDoc.
- Hugo template.
- Hugo Coder theme template.
- Home, Usage, and Credits pages.

### Changed
- Update readme documentation.

## 0.1.0 - 2018-05-05
### Added
- Changelog, license, readme.
- Gradle JOSM plugin demo.
- Batch Circle Building action.
- Batch Orthogonal Building action.
- Pick Residential Area action.
- Mapathoner main class.

### Changed
- Update `build.gradle` with actual project info.

[Unreleased]: https://gitlab.com/qeef/mapathoner/compare/v0.10.1...master
[0.10.1]: https://gitlab.com/qeef/mapathoner/compare/v0.10.0...v0.10.1
[0.10.0]: https://gitlab.com/qeef/mapathoner/compare/v0.9.8...v0.10.0
[0.9.8]: https://gitlab.com/qeef/mapathoner/compare/v0.9.7...v0.9.8
[0.9.7]: https://gitlab.com/qeef/mapathoner/compare/v0.9.6...v0.9.7
[0.9.6]: https://gitlab.com/qeef/mapathoner/compare/v0.9.5...v0.9.6
[0.9.5]: https://gitlab.com/qeef/mapathoner/compare/v0.9.4...v0.9.5
[0.9.4]: https://gitlab.com/qeef/mapathoner/compare/v0.9.3...v0.9.4
[0.9.3]: https://gitlab.com/qeef/mapathoner/compare/v0.9.2...v0.9.3
[0.9.2]: https://gitlab.com/qeef/mapathoner/compare/v0.9.1...v0.9.2
[0.9.1]: https://gitlab.com/qeef/mapathoner/compare/v0.9.0...v0.9.1
[0.9.0]: https://gitlab.com/qeef/mapathoner/compare/v0.8.7...v0.9.0
[0.8.7]: https://gitlab.com/qeef/mapathoner/compare/v0.8.6...v0.8.7
[0.8.6]: https://gitlab.com/qeef/mapathoner/compare/v0.8.5...v0.8.6
[0.8.5]: https://gitlab.com/qeef/mapathoner/compare/v0.8.4...v0.8.5
[0.8.4]: https://gitlab.com/qeef/mapathoner/compare/v0.8.3...v0.8.4
[0.8.3]: https://gitlab.com/qeef/mapathoner/compare/v0.8.2...v0.8.3
[0.8.2]: https://gitlab.com/qeef/mapathoner/compare/v0.8.1...v0.8.2
[0.8.1]: https://gitlab.com/qeef/mapathoner/compare/v0.8.0...v0.8.1
[0.8.0]: https://gitlab.com/qeef/mapathoner/compare/v0.7.8...v0.8.0
[0.7.8]: https://gitlab.com/qeef/mapathoner/compare/v0.7.7...v0.7.8
[0.7.7]: https://gitlab.com/qeef/mapathoner/compare/v0.7.6...v0.7.7
[0.7.6]: https://gitlab.com/qeef/mapathoner/compare/v0.7.5...v0.7.6
[0.7.5]: https://gitlab.com/qeef/mapathoner/compare/v0.7.4...v0.7.5
[0.7.4]: https://gitlab.com/qeef/mapathoner/compare/v0.7.3...v0.7.4
[0.7.3]: https://gitlab.com/qeef/mapathoner/compare/v0.7.2...v0.7.3
[0.7.2]: https://gitlab.com/qeef/mapathoner/compare/v0.7.1...v0.7.2
[0.7.1]: https://gitlab.com/qeef/mapathoner/compare/v0.7.0...v0.7.1
[0.7.0]: https://gitlab.com/qeef/mapathoner/compare/v0.6.5...v0.7.0
[0.6.5]: https://gitlab.com/qeef/mapathoner/compare/v0.6.4...v0.6.5
[0.6.4]: https://gitlab.com/qeef/mapathoner/compare/v0.6.3...v0.6.4
[0.6.3]: https://gitlab.com/qeef/mapathoner/compare/v0.6.2...v0.6.3
[0.6.2]: https://gitlab.com/qeef/mapathoner/compare/v0.6.1...v0.6.2
[0.6.1]: https://gitlab.com/qeef/mapathoner/compare/v0.6.0...v0.6.1
[0.6.0]: https://gitlab.com/qeef/mapathoner/compare/v0.5.17...v0.6.0
[0.5.17]: https://gitlab.com/qeef/mapathoner/compare/v0.5.16...v0.5.17
[0.5.16]: https://gitlab.com/qeef/mapathoner/compare/v0.5.15...v0.5.16
[0.5.15]: https://gitlab.com/qeef/mapathoner/compare/v0.5.14...v0.5.15
[0.5.14]: https://gitlab.com/qeef/mapathoner/compare/v0.5.13...v0.5.14
[0.5.13]: https://gitlab.com/qeef/mapathoner/compare/v0.5.12...v0.5.13
[0.5.12]: https://gitlab.com/qeef/mapathoner/compare/v0.5.11...v0.5.12
[0.5.11]: https://gitlab.com/qeef/mapathoner/compare/v0.5.10...v0.5.11
[0.5.10]: https://gitlab.com/qeef/mapathoner/compare/v0.5.9...v0.5.10
[0.5.9]: https://gitlab.com/qeef/mapathoner/compare/v0.5.8...v0.5.9
[0.5.8]: https://gitlab.com/qeef/mapathoner/compare/v0.5.7...v0.5.8
[0.5.7]: https://gitlab.com/qeef/mapathoner/compare/v0.5.6...v0.5.7
[0.5.6]: https://gitlab.com/qeef/mapathoner/compare/v0.5.5...v0.5.6
[0.5.5]: https://gitlab.com/qeef/mapathoner/compare/v0.5.4...v0.5.5
[0.5.4]: https://gitlab.com/qeef/mapathoner/compare/v0.5.3...v0.5.4
[0.5.3]: https://gitlab.com/qeef/mapathoner/compare/v0.5.2...v0.5.3
[0.5.2]: https://gitlab.com/qeef/mapathoner/compare/v0.5.1...v0.5.2
[0.5.1]: https://gitlab.com/qeef/mapathoner/compare/v0.5.0...v0.5.1
[0.5.0]: https://gitlab.com/qeef/mapathoner/compare/v0.4.2...v0.5.0
[0.4.2]: https://gitlab.com/qeef/mapathoner/compare/v0.4.1...v0.4.2
[0.4.1]: https://gitlab.com/qeef/mapathoner/compare/v0.4.0...v0.4.1
[0.4.0]: https://gitlab.com/qeef/mapathoner/compare/v0.3.0...v0.4.0
[0.3.0]: https://gitlab.com/qeef/mapathoner/compare/v0.2.4...v0.3.0
[0.2.4]: https://gitlab.com/qeef/mapathoner/compare/v0.2.3...v0.2.4
[0.2.3]: https://gitlab.com/qeef/mapathoner/compare/v0.2.2...v0.2.3
[0.2.2]: https://gitlab.com/qeef/mapathoner/compare/v0.2.1...v0.2.2
[0.2.1]: https://gitlab.com/qeef/mapathoner/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/qeef/mapathoner/compare/v0.1.0...v0.2.0
